
# The PROTOCOL_MAY

Consent Solution for GDPR Lawful processing of personal data

## ACTORS:

1. Data Subject - Data Owner: The user whose personal information is being processed.

2. Data Controller: Any industry that collects personal information, such as consumer IoTs and electronic communication [ a person or organization which determines the purposes and means o the processing of personal data].

3.  Data Processor : is a person or organization which processes personal data on behalf of the Controller.

  

## ABSTRACT

The recent personal data GDPR regulations effective as of May 25, 2018 require consent for all treatment and processing of personalised data. GDPR created at least two challenges, the first is the fact that Data Processors need to generate hundreds of consent forms for every time they want to use the data set for a specific purpose, and with such written contracts- the the validity of the collected consent, depending on their phrasing, interface options, and etc., cannot be guaranteed. The second challenge is that the Data Owners will now be overburdened with consent forms leading to them to approving everything in its default status. We propose a blockchain solution that provides one location where people could specify the level of privacy they would like and what behavior they would like the Data Processors and Controllers to follow (i.e. data type, frequency, and etc.). This will relieve the DPs and DCs from sending requests for every alteration and will provide a live matrix of data settings for each Data Owner. The main idea behind this protocol is that consent can only be given once someone truly understands the implication of its decision. Using this protocol we hope to standardise the type of consent people can agree to and have them express it once and for all, making it easier for them to understand what they are agreeing to. It is then the responsibility of the Data controller to follow the preferences of the data owner.

  
  
  

--------

  
  
  

## INTRODUCTION:

Recent personal data GDPR regulations issued on April 27, 2016 and effective as of May 25, 2018, require that any person or organization processing data relating to an identified or identifiable person, data subject or data owner, needs to have legal grounds for data processing. Thus, consent needs to meet a strict criteria : it needs to be informed, given freely, specific, and given in an affirmative action. In addition, a manufacturer or service needs to provide information about the nature of processing, the purposes of processing, and the name of the organization that requested processing.

  

Some of the main challenges will be assuring that the Data Owner can clearly understand the requests from the Data controller and thus, the “informed” consent may always be subject to a challenge. In addition, any time a Data Controller will add a new feature to the device or propose a new service- it will need to not only correctly inform the Data Owner and get a consent, but also demonstrate that the consent collected matches the service provided. Given that an average Data Owner interacts with more than 20 different Data Controllers and with more than 1000 Data Processors a day - how can this process be optimised for all of the participating parties?

  

We propose that Data Owners once and for all which behaviour they would like per data type, frequency, etc. Although multiple solutions could exist but we believe that Blockchain is the best technological solution to address this problem.

  

Using Smart Contracts on the blockchain, the protocol described in this document, Protocol_MAY, enables the Data Owner to define the privacy settings per data type once and for all. When setting up an app/IoT appliance, it will ask the Data owner which contract describe its consent (classified by data type/properties). The app/appliance would look for the action pre-agreed on by the data owner, and follow the instructions. The appliance would have to monitor the contract to check whether any changes happened in the consentment. But users would have one and only one place to go to to specify their consent..

  
  

Protocol_MAY proposes a GDPR compliant consent protocol, where:

1.  Data Owners assign specific actions to each data label, and store it in smart contracts
    
2.  Data Collector fills labels and fills in the data they acquire by the collecting device
    
3.  Collecting devices request Data Owners where there can find their expression of consent.
    
4.  Collecting devices will be able to request which actions they are allowed to perform with the data according to the type of data they collect..
    

  
  

## METHOD:

With the new blockchain technology and smart contracts the Protocol_May consent steps can now be automatically executed and tracked, thus providing traceable and real time logs of all the settings and executed actions. Data Owners can thereby easily select which action they want to correspond to the specific data set, and the Data controller will act accordingly. This will improve the Data Controller situation as well, hence every possible action would become standardised and they would only have to follow the standard to become GDPR compliant...

  

Step 1: [Labeling Data Content] The Data  Controller labels the collected data. And writes it in to the collecting device

  

Step 2: [Privacy Setting] Data  Owner either defines actions appropriate for his/her preferred level of privacy and stores it in a smart contract, or picks a smart contract that already has actions assigned to specific combinations of labels.

  

Step 3: [Action Execution] Data Controller’s device request from the data Owner the address of the Smart Contract where the actions allowed are stored, retrieve the actions that match his/her combination of Data Labels, and then follows said actions.

  
  

###  Detailed Methods:

**Step 1**: [Labeling Data Content].

GDPR requires Data Controllers to specify the type of data collected and its usage. To do that, we propose a creation of a multidimensional matrix (Smart Contract 1) that lists the type of data collected and the details about how it is collected. The Data Controller will then program the collecting device to use these labels to request the type of actions the users agreed to.

Here are a few examples of the Labels.

 - Type of Data:

	a. Behavior

	b. Health

	c. Environmental

	d. Social

	e. Culture

	f. Other

 - Origin: Data collection device

 -  Sensor Types
     

	 - Gyroscope   	    
	  - Barometer   	    
	   - Magnetometer   	    
	   	- Thermometer   	    
	   	- Proximity   	    
	   	 - Ambient Light   	    
	   	- Accelerometers   	    
		 -  Optical   	    
	   	 -  Humidity   	    
	   	 -  Biometrics

    

- Storage Duration Time : Exact length of time the data will be stored

- Collection Duration : How long will the sensor be collecting information

- Collection Time : Exact times the data will be collected

- Collection Frequency : How frequently the data will be collected

- Purpose

- Research

- For-Profit

-  Non-Profit

- Manufacturing

- Diagnostics

- Other

This labeling system attempts to address points in Article 5 of the GDPR - points 1a, 1b, 1c, 1d [ 1a. lawful, fair, transparent processing of the data, 1b: collected for specified and explicit purposes, 1c: data minimisation, 1d: accurate and up to date].

[figure : matrix with all the labels and sub-labes]

We propose a design based on two different type of Smart Contract, living on a Blockchain like Ethereum, which purpose is to provide Collecting devices and Data Controllers the pre-agreed description of what they can do with the data.

### Smart Contract 1

The purpose of SC1 is to know what data is being collected by the Data Controller and for the Data Owner to define what he/she wants to be done with the data. The latter, could be done by the Data Owners on their own, or they could follow the standards developed by various organisations.

SC1 will consist of a multidimensional array of all the possible Labels and the link to the action corresponding to each possible combinations.. This will enable any Data Controller to efficiently search the array for the type of information it is collecting.

For example, if a temperature sensor on a wrist bracelet collects data once an hour, then it will fill-in the label : Origin: wrist bracelet (model X), Sensor  type : thermometer, Collection Frequency : 1x / hour.

The combinations of labels (intersections) will be filled-in, by either by the DO or predefined by an organisation, with references to specific Smart Contract 2s addresses. Each SC2 contains the instructions on what action needs to be executed by the Data Controller.

For example, if the Data Controller wants his Privacy Setting to be really protective, then the SC1 at any intersection of labels will be referring to a specific SC2 with an action Delete. Therefore instructing the Data Controller to delete the data. On the other hand, if the user wants her/his health data to be used for research purposes, then the intersections of the labels will call upon SC2 with the instruction to Encrypt and send to a specific location.

This contract will enable Data Controllers to easily keep track of the data they are collecting, get an automatic consent from Data Owners, and easily modify their settings.

[Figure: multidimensional matrix with filled in intersections!]

### Smart Contract 2

Here are some examples of 7 SC2s corresponding to actions that could be taken by the Data Controller for data treatment:

1. [a1] Delete Data

2. Encrypt Data (type of encryption , keys, etc.)

3. Send Data to Server X (address of server, authentication mechanism, etc.)IoT

4. Send Data to another device

5. Save Data on the Market Place.

6. Authentication

7. Ask the User

8. ETC.

Of course, as the protocol gets modified and used - there will be more and more actions added to the list.

## List of Methods:

### Smart Contract 1:
```
setAction(listOfSettings, action)
```

This method is used for registering an action at the crossing of a listOfSettings. Only accessible by the owner of the contract

```  
setDefaultAction (action)
```

Defines the wildcard action for every single dimension.

```
lockContract ()
```
Lock the contract into the current state. This method is intended to be called upon by associations who defined specific contract behaviors and want to allow people to use them while ensuring that they change the behavior later in time.

```
terminateContract([redirection])
```

It is the only function allowed to do modification after lockContract() has been called. Indicate that the contract is not considered valid anymore. Supposed to be used when a contract needs to be deprecated but lockContract() has been called. Any data collector connected to the contract on which this function has been called should prompt its users to provide a new consent contract. The *redirection* parameter informs allows for passing a new contract to be considered as the source of consent.

```
transferContract(newOwner)
```

Transfers the ownership of the contract to the *newOwner*.

```
getAction (listOfSettings [, mask])
```

Returns an action matching a *lilstOfSettings*. It is possible to specify a mask mentioning which settings don't need to be a perfect match. When provided and there is no perfect match to *listOfSettings*, the matching function goes through the list of possibilities to see whether there is a match. If not returns the defaultAction.

## CONCLUSION:

  
The main idea behind this protocol is that consent can only be given once someone truly understands the implication of its decision. Using this protocol we hope to standardise the type of consent people can agree to and have them express it once and for all, making it easier for them to understand what they are agreeing to. It is then the responsibility of the Data controller to follow the preferences of the data owner. But as the expected behavior of the Data Controller will also be thoroughly specified, it would be easier for them to know whether they are GDPR compliant or no.This will enable Data Owner to define his or her level of privacy and thereby provide consent to the processing of his or he r personal data for one or more specific labels. Data Controllers will only have to label the type of data they collect and follow the according instructions.

  ____________
  ____________  
  

Current Interaction between the IoT Actors:

[diagram 1: User IoT interaction ]

[diagram 1 description: User sends the data to the IoT device. IoT device then treats the data based on the internal policies of the company. The User has no idea how his company is used or to whom it has been sent to.]

[diagram 2: User –Iot Interaction with smart contract]

[diagram 2 description: User sets the rules on the SC on how he wants his data to be used. The IoT device uses SC to receive in-vivo instructions on how the data from the User should be used. IoT device then treats the data accordingly to the conditions of the SC.]

  
  

Smart Contract:

The program runs this code and at some point it automatically validates a condition and it automatically determines whether the asset should go to one person or back to the other person, or whether it should be immediately refunded to the person who sent it or some combination thereof.

Autonomy – You’re the one making the agreement; there’s no need to rely on a broker, lawyer or other intermediaries to confirm. Incidentally, this also knocks out the danger of manipulation by a third party, since execution is managed automatically by the network, rather than by one or more, possibly biased, individuals who may err.

Trust – Your documents are encrypted on a shared ledger. There’s no way that someone can say they lost it. -> Can’t collected if it’s not listed in the SC1 and SC2.

Backup – Imagine if your bank lost your savings account. On the blockchain, each and every one of your friends has your back. Your documents are duplicated many times over.

Safety – [Cryptography](https://blockgeeks.com/guides/cryptocurrencies-cryptography/), the encryption of websites, keeps your documents safe. There is no hacking. In fact, it would take an abnormally smart hacker to crack the code and infiltrate.

Speed – You’d ordinarily have to spend chunks of time and paperwork to manually process documents. Smart contracts use software code to automate tasks, thereby shaving hours off a range of business processes.

Savings – Smart contracts save you money since they knock out the presence of an intermediary. You would, for instance, have to pay a notary to witness your transaction.

Accuracy – Automated contracts are not only faster and cheaper but also avoid the errors that come from manually filling out heaps of forms.

----------

[a1]You should mention that this part is really open, what is contained in it is explaination of how to deal with the data. Maybe one of the field could be the standard used for describing the action
