pragma solidity ^0.4.18;

contract Contract1{

	bool public _locked;
	bool public _active;

	address public _owner;
	address public _defaultAction;
	address public _redirection;

	uint256 _version;

	uint256[] public listOfMapping;

	mapping (uint256 => address) public _actionMap;

	/* Modifiers */

	modifier onlyOwner(){
		require(msg.sender == _owner);
		_;
	}

	modifier unlocked(){
		require (!_locked);
		_;
	}

	/* Events */

	event contractTerminated(address redirection);

	/* Constructor */

	function Contract1 (address defaultAction, uint256 version) public {
		_owner = msg.sender;
		_active = true;
		_locked = false;
		_defaultAction = defaultAction;
		_version = version;

	}
	/* Internal functions */

	function unMasked(uint256 number, uint256 mask) internal pure returns (uint256 masked){
		// Returns the number minus the bits where the mask is at 1
		uint256 notMask;
		notMask = 0 - (mask +1);
		return (number & notMask);
	}

	function proximity (uint256 number1, uint256 number2) internal pure returns (uint256 proximity){
		uint256 proximity = 0;
		uint256 currentBit = 1;

		for (uint256 i = 0; i < 256 ; i++){
			if ((number1 & currentBit) == (number2 & currentBit){
				if ((number1 & currentBit) != 0){
					proximity++;
				}
			}
			currentBit = currentBit * 2;
		}
		return proximity;

	}

	/* External function */

	function setAction(uint256 listOfSettings, address action) public onlyOwner() unlocked(){
		if (_actionMap[listOfSettings] = 0){
			listOfMapping.push(listOfSettings);
		}
		_actionMap[listOfSettings] = action;
	}

	function setDefaultAction (address action) public onlyOwner() unlocked(){
		_defaultAction = action;
	}

	function lockContract() public onlyOwner() unlocked(){
		_locked = true;
	}

	function terminateContract(address redirection) public onlyOwner() {
		_active = false;
		_locked = true;
		_redirection = redirection;
		contractTerminated(redirection);
	}

	function terminateContract() public onlyOwner() {
		_active = false;
		_locked = true;
		contractTerminated(0x0);
	}

	function transferContract (address newOwner) public onlyOwner() unlocked(){
		_owner = newOwner;
	}

	function getAction(uint256 listOfSettings) public pure unlocked() returns (address action){
		if (_actionMap[listOfSettings] != 0){
			return _actionMap[listOfSettings];
		}
		else {
			return _defaultAction;
		}
	}

	function getAction (uint256 listOfSettings, uint256 mask) public pure unlocked() returns (address action){
		
		// If we have a perfect match we don't take the mask into account
		if (_actionMap[listOfSettings] != 0){
			return _actionMap[listOfSettings];
		}

		// Otherwise we test different possibilities implied by the mask
		else{
			uint256[] match;
			// Search all settings combination matching the searched setting
			for (uint256 i = 0; i < listOfMapping.length; i++){
				if (unMasked(listOfMapping[i],mask) == unMasked(listOfSettings,mask)){
					match.push(listOfMapping);
				}
			}
			// of all the results returns the one closest to the original settings combination

			uint256 maxDist = 0;
			address winner;
			for (uint256 i = 0; i < match.length; i++){
				if (proximity(listOfSettings, match[i]) > maxDist){
					winner = match[i];
				}
			}
			if (winner == 0){
				return _defaultAction;
			}
			return winner;
		}
	}


}